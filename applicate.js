const art = require('art-template');
const db = require('./database/db');
art.defaults.root='./';

module.exports = {
  getAll:function (req,res) {
      db.select(function (datas) {
          let html_data = art('./view/index.html',{data:datas});
          res.end(html_data);
      })
  },

  add:function (req, res) {
      db.insert(function (datas) {
          let html_data = art('./view/add.html',{data:datas});
          res.end(html_data);
      })
  }
};
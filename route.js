const url = require('url');
const fs = require('fs');
const app = require('./applicate');

module.exports.binding = function (server) {
    server.on('request',function (request, response) {
        //解析请求地址
        let urlObj = url.parse(request.url,true);
        if(request.method === 'GET'){
            if(urlObj.pathname === '/'){
                app.getAll(request,response);
                // response.end("<html><h1>jiijiji</h1></html>")
            }else{
                fs.readFile('.' + urlObj.pathname, function (err, data) {
                    response.end(data);
                });
            }
        }else if (request.method === 'POST'){

        }else{
            response.end('不支持请求');
        }
    });
};